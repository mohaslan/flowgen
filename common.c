/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdarg.h>
#include <err.h>
#include <sys/event.h>
#include "common.h"


/*
 * one shot timer
 */
void
atimer(int ev_queue, int id, unsigned int t)
{
	struct kevent           kev;

	EV_SET(&kev, id, EVFILT_TIMER, EV_ADD | EV_ENABLE | EV_ONESHOT, 0, t, NULL);
	if ((kevent(ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
		err(1, "timer");
}

/*
 * periodic timer with period p
 */
void
timer(int ev_queue, int id, unsigned int p)
{
	struct kevent           kev;

	EV_SET(&kev, id, EVFILT_TIMER, EV_ADD | EV_ENABLE, 0, p, NULL);
	if ((kevent(ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
		err(1, "timer");
}

void
log_init(struct log *lg)
{
	if (lg->type == LOG_CSV) {
		if ((lg->fd = fopen(lg->param.filename, "w")) == NULL)
			errx(1, "error creating log file.");
	}
}

void
log_free(struct log *lg)
{
	if (lg->type == LOG_CSV)
		fclose(lg->fd);
}

void
f_log(struct log *lg, const char *fmt, ...)
{
	va_list			ap;

	va_start(ap, fmt);
	if (lg->type == LOG_CSV) {
		if (vfprintf(lg->fd, fmt, ap) < -1)
			errx(1, "vfprintf");
	}
	va_end(ap);
}
