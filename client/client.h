/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef CLIENT_H
#define CLIENT_H

#include <netinet/in.h>

#define MAX_FLOWS		256
#define TIMER_RES		0.001		/* 1 ms */
#define DEFAULT_CONFIG		"flowgen.conf"


struct flow {
	int			 id;
	double			 at;
	double			 to;
	char			*dest_ip;
	uint16_t		 dest_port;
	enum {
		EVT_TCP,
		EVT_UDP
	} transport;
	enum {
		EVP_CONSTANT,
		EVP_VARIABLE			/* XXX: NOT SUPPORTED */
	} payload;
	union {
		uint16_t	 size;
		double		 mean;		/* XXX: NOT SUPPORTED */
	} payload_param;
	enum {
		EVI_EXPONENTIAL,
		EVI_PARETO,
		EVI_CBR,			/* XXX: NOT SUPPORTED */
		EVI_CUSTOM			/* XXX: NOT SUPPORTED */
	} interarrival;
	union {
		double		 mean;		/* used by EVI_EXPONENTIAL */
		struct {
			double	 alpha;
			double	 beta;
		} alphabeta;			/* used by EVI_PARETO */
	} interarrival_param;
	/* used by client.c */
	char			*buf;
	int		 	 skt;
	double			 now;
	struct sockaddr_in	 sa;
	int			 (*handler)(int, struct flow *, struct log *);
};

/* client.c */
int	client_main(int, char **);
/* client.y */
int	client_parse_config(const char *, struct flow *, int *, struct log *);

#endif
