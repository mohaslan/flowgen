LEX=lex
YACC=yacc
CFLAGS+= -Wall -I/usr/include/kqueue -I. -g -DDEBUG
LDFLAGS+= -lkqueue -L. -lbsd -lm
SRCS=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SRCS))
all: flowgen
%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)
lex.yy.c: lex.l
	$(LEX) $<
y.tab.c y.tab.h: parse.y
	$(YACC) -d $<
flowgen: $(OBJS) client.o y.tab.o lex.yy.o ../common.o
	cc -o flowgen client.o y.tab.o lex.yy.o ../common.o $(LDFLAGS)
clean:
	rm -f *.o *.yy.c *.tab.c *.tab.h flowgen
