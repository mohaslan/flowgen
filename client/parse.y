/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

%{
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <err.h>
#include <stdint.h>

#include "../common.h"
#include "client.h"

/* lex vars */
extern FILE		*yyin;
extern int	 	 yylineno;
extern int	 	 yylex(void);
/* global vars */
int		 	 yyerror(const char *, ...);
static char		*filename;
static struct flow	*flows;
static struct log	*log;
static int		 cur_flow = -1;
%}

%union {
	int			 i;
	double			 d;
	char			*s;
}

%token		AT TO FOR IS AND WITH
%token		DESTINATION IP PORT HOST
%token		TRANSPORT UDP TCP
%token		PAYLOAD CONSTANT SIZE
%token		INTERARRIVAL EXPONENTIAL MEAN PARETO SHAPE SCALE
%token		LOG CSV
%token		LCBRACKET RCBRACKET

%token<i>	INTEGER
%token<d>	REAL
%token<s>	STRING
%token<s>	IPADDR

%type<d>	number

%%

main		:
		| main flow
		| main log
		;

flow		: at LCBRACKET conf RCBRACKET
		;

conf		:
		| conf destination
		| conf transport
		| conf payload
		| conf interarrival
		;


destination	: DESTINATION IS IP IPADDR AND PORT INTEGER {
			flows[cur_flow].dest_ip = strdup($4);	/* XXX: won't be checked here */
			if ($7 < 1 || $7 > 65535) {
				yyerror("invalid port number '%d'.", $7);
				YYABORT;
			}
			flows[cur_flow].dest_port = (uint16_t)$7;
		}
		| DESTINATION IS HOST STRING AND PORT INTEGER {
			flows[cur_flow].dest_ip = strdup($4);
			if ($7 < 1 || $7 > 65535) {
				yyerror("invalid port number '%d'.", $7);
				YYABORT;
			}
			flows[cur_flow].dest_port = (uint16_t)$7;
		}
		;

transport	: TRANSPORT IS TCP {
			flows[cur_flow].transport = EVT_TCP;
		}
		| TRANSPORT IS UDP {
			flows[cur_flow].transport = EVT_UDP;
		}
		;

payload		: PAYLOAD IS CONSTANT WITH SIZE INTEGER {
			flows[cur_flow].payload = EVP_CONSTANT;
			flows[cur_flow].payload_param.size = $6;
		}
		;

interarrival	: INTERARRIVAL IS EXPONENTIAL WITH MEAN REAL {
			flows[cur_flow].interarrival = EVI_EXPONENTIAL;
			flows[cur_flow].interarrival_param.mean = $6;
		}
		| INTERARRIVAL IS PARETO WITH SHAPE REAL AND SCALE REAL {
			flows[cur_flow].interarrival = EVI_PARETO;
			flows[cur_flow].interarrival_param.alphabeta.alpha = $6;
			flows[cur_flow].interarrival_param.alphabeta.beta = $9;
		}
		;

number		: INTEGER {
			$$ = (double)$1;
		}
		| REAL {
			$$ = $1;
		};

at		: AT number TO number {
			if ($2 >= $4) {
				yyerror("invalid flow time '%d' -> '%d'.", $2, $4);
				YYABORT;
			}
			++cur_flow;
			if (cur_flow >= MAX_FLOWS) {
				yyerror("maximum number of servers reached (%d).", cur_flow);
				YYABORT;
			}
			flows[cur_flow].id = cur_flow;
			flows[cur_flow].at = $2;
			flows[cur_flow].to = $4;
		}
		| AT number FOR number {
			if ($2 >= $4) {
				yyerror("invalid flow time '%d' -> '%d'.", $2, $2 + $4);
				YYABORT;
			}
			++cur_flow;
			if (cur_flow >= MAX_FLOWS) {
				yyerror("maximum number of servers reached (%d).", cur_flow);
				YYABORT;
			}
			flows[cur_flow].id = cur_flow;
			flows[cur_flow].at = $2;
			flows[cur_flow].to = $2 + $4;
		}
		;

log		: LOG IS CSV STRING {
			log->type = LOG_CSV;
			log->param.filename = strdup($4);
		}
		;
%%

int
yyerror(const char *fmt, ...)
{
	va_list	ap;

	va_start(ap, fmt);
	fprintf(stdout, "%s:%d: ", filename, yylineno);
	vfprintf(stdout, fmt, ap);
	fprintf(stdout, "\n");
	va_end(ap);

	return 0;
}

int
parse_config(const char *file, struct flow *fs, int *n_flows, struct log *lg)
{
	int	ret;

	if (fs == NULL || lg == NULL)
		errx(1, "parse_config");
	if ((yyin = fopen(file, "r")) == NULL) {
		errx(1, "failed to load config file");
	}
	filename = strdup(file);
	flows = fs;
	log = lg;
	*n_flows = 0;
	ret = yyparse();
	fclose(yyin);
	if (cur_flow != -1)
		*n_flows = cur_flow + 1;
	free(filename);
	return ret;
}
