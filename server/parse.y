/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

%{
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <err.h>
#include <stdint.h>

#include "../common.h"
#include "server.h"

/* lex vars */
extern FILE		*yyin;
extern int	 	 yylineno;
extern int	 	 yylex(void);
/* global vars */
int		 	 yyerror(const char *, ...);
static char		*filename;
static struct server	*servers;
static struct log	*log;
static int		 cur_server = -1;
%}

%union {
	int			 i;
	double			 d;
	char			*s;
}

%token		AT TO FOR IS AND WITH
%token		LISTEN ON PORT
%token		TRANSPORT UDP TCP
%token		BUFFER SIZE
%token		LOG CSV
%token		LCBRACKET RCBRACKET

%token<i>	INTEGER
%token<d>	REAL
%token<s>	STRING
%token<s>	IPADDR

%type<d>	number

%%

main		:
		| main server
		| main log
		;

server		: at LCBRACKET conf RCBRACKET
		;

conf		:
		| conf listen 
		| conf transport
		| conf buffer
		;


listen		: LISTEN ON PORT INTEGER {
			if ($4 < 1 || $4 > 65535) {
				yyerror("invalid port number '%d'.", $4);
				YYABORT;
			}
			servers[cur_server].port = (uint16_t)$4;
		}
		;

transport	: TRANSPORT IS TCP {
			servers[cur_server].transport = SRVT_TCP;
		}
		| TRANSPORT IS UDP {
			servers[cur_server].transport = SRVT_UDP;
		}
		;

buffer		: BUFFER SIZE IS INTEGER {
			if ($4 < 1 || $4 > 65535) {
				yyerror("invalid buffer size '%zu'.", $4);
				YYABORT;
			}
			servers[cur_server].buflen = (size_t)$4;
		}
		;

number		: INTEGER {
			$$ = (double)$1;
		}
		| REAL {
			$$ = $1;
		};

at		: AT number TO number {
			if ($2 >= $4) {
				yyerror("invalid server time '%d' -> '%d'.", $2, $4);
				YYABORT;
			}
			++cur_server;
			if (cur_server >= MAX_SERVERS) {
				yyerror("maximum number of servers reached (%d).", cur_server);
				YYABORT;
			}
			servers[cur_server].id = cur_server;
			servers[cur_server].at = $2;
			servers[cur_server].to = $4;
		}
		| AT number FOR number {
			if ($2 >= $4) {
				yyerror("invalid server time '%d' -> '%d'.", $2, $2 + $4);
				YYABORT;
			}
			++cur_server;
			if (cur_server >= MAX_SERVERS) {
				yyerror("maximum number of servers reached (%d).", cur_server);
				YYABORT;
			}
			servers[cur_server].id = cur_server;
			servers[cur_server].at = $2;
			servers[cur_server].to = $2 + $4;
		}
		;

log		: LOG IS CSV STRING {
			log->type = LOG_CSV;
			log->param.filename = strdup($4);
		}
		;
%%

int
yyerror(const char *fmt, ...)
{
	va_list	ap;

	va_start(ap, fmt);
	fprintf(stdout, "%s:%d: ", filename, yylineno);
	vfprintf(stdout, fmt, ap);
	fprintf(stdout, "\n");
	va_end(ap);

	return 0;
}

int
parse_config(const char *file, struct server *srvs, int *n_servers, struct log *lg)
{
	int	ret;

	if (srvs == NULL || lg == NULL)
		errx(1, "parse_config");
	if ((yyin = fopen(file, "r")) == NULL) {
		errx(1, "failed to load config file");
	}
	filename = strdup(file);
	servers = srvs;
	log = lg;
	*n_servers = 0;
	ret = yyparse();
	fclose(yyin);
	if (cur_server != -1)
		*n_servers = cur_server + 1;
	free(filename);
	return ret;
}
