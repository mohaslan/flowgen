/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef SERVER_H
#define SERVER_H

#include <netinet/in.h>

#define MAX_SERVERS		100
#define TCPBACKLOG		500
#define DEFAULT_CONFIG		"flowgend.conf"

struct server {
	int			 id;
	double			 at;
	double			 to;
	uint16_t		 port;
	enum {
		SRVT_TCP,
		SRVT_UDP
	} transport;
	/* used by tgens.c */
	char			*buf;
	size_t			 buflen;
	int			 sskt;
	int			 started;
	double			 now;
	struct sockaddr_in	 sa;
	int			 (*handler)(int, struct server *, struct log *);
};

/* server.c */
int	server_main(int, char **);
/* server.y */
int	server_parse_config(const char *, struct server *, int *, struct log *);

#endif
