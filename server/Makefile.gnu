LEX=lex
YACC=yacc
CFLAGS+= -Wall -I/usr/include/kqueue -I. -g -DDEBUG
LDFLAGS+= -lkqueue -L. -lbsd -lm -lpthread
SRCS=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SRCS))
all: flowgend
%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)
lex.yy.c: lex.l
	$(LEX) $<
y.tab.c y.tab.h: parse.y
	$(YACC) -d $<
flowgend: $(OBJS) server.o y.tab.o lex.yy.o ../common.o
	cc -o flowgend server.o y.tab.o lex.yy.o ../common.o $(LDFLAGS)
clean:
	rm -f *.o *.yy.c *.tab.c *.tab.h flowgend
