/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#include "server.h"
#include "../common.h"

#include <sys/ioctl.h>

static void
print_info(struct server *s)
{
	printf("INFO: server %d.\n", s->id);
	printf("INFO: at %lf to %lf.\n", s->at, s->to);
	printf("INFO: port: %u.\n", s->port);
	if (s->transport == SRVT_UDP)
		printf("INFO: transport: UDP.\n");
	else if (s->transport == SRVT_TCP)
		printf("INFO: transport: TCP.\n");
}

static void
server_init(struct server *s)
{
	s->started = 0;
	s->buflen = 1024;
}

static void
server_postinit(struct server *s)
{
	/* ip and port */
	memset(&s->sa, 0, sizeof s->sa);
	s->sa.sin_family = AF_INET;
	s->sa.sin_port = htons(s->port);
	s->sa.sin_addr.s_addr = htonl(INADDR_ANY);

	/* buffer */
	s->buf = (char *)malloc(s->buflen);
}

static void
server_start(int ev_queue, struct server *s)
{
	struct kevent		kev;

#ifdef DEBUG
	printf("[INFO] starting server %d.\n", s->id);
#endif
	/* server socket */
	if (s->transport == SRVT_TCP) {
		if ((s->sskt = socket(AF_INET, SOCK_STREAM, 0)) == -1)
			err(1, "socket");
		printf("[INFO] TCP.\n");
	} else if (s->transport == SRVT_UDP) {
		if ((s->sskt = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
			err(1, "socket");
		printf("[INFO] UDP.\n");
	}
	/* bind and listen */
	if (bind(s->sskt, (struct sockaddr *)&s->sa, sizeof(s->sa)) == -1)
		err(1, "bind");
	if (s->transport == SRVT_TCP) {
		if (listen(s->sskt, TCPBACKLOG) == -1)
			err(1, "listen");
		printf("[INFO] listen!.\n");
	}
	s->started = 1;
	/* add to event queue */
	/* EV_SET(&kev, s->sskt, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, (void *)s); */
	EV_SET(&kev, s->sskt, EVFILT_READ, EV_ADD, 0, 0, (void *)s);
	if ((kevent(ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
		err(1, "kevent");
	/* schedule a stoppage event */
	atimer(ev_queue, s->id, (unsigned int)((s->to - s->at) * 1000)); /* server.y guarantees: to > at */
}

static void
server_stop(int ev_queue, struct server *s)
{
	struct kevent		kev;

#ifdef DEBUG
	printf("[INFO] stopping server %d.\n", s->id);
#endif
	EV_SET(&kev, s->sskt, EVFILT_READ, EV_DELETE, 0, 0, NULL);
	if ((kevent(ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
		err(1, "kevent");
	close(s);
}

static void
events_init(int ev_queue, struct server *s, struct log *lg)
{
	if (s->at == 0)
		server_start(ev_queue, s);
	else
		atimer(ev_queue, s->id, (unsigned int)(s->at * 1000));
}

int
main(int argc, char **argv)
{
	char			*conf_file = NULL;
	int			 ch, i, n, s, *ip;
	int			 client_skt;
	int			 ev_queue;
	ssize_t			 bytes;
	struct sockaddr_storage	 sw_addr;
	socklen_t		 addr_len;
	struct kevent		 kev;
	struct server		 servers[MAX_SERVERS], *sp;
	struct log		 log;
	int			 n_servers;

	struct timeval		start, now;
	double			evtm;
	unsigned long long	a, b;



	/* parse command line options */
	while ((ch = getopt(argc, argv, "f:")) != -1) {
		switch (ch) {
		case 'f':
			conf_file = strdup(optarg);
			break;
		default:
			errx(1, "invalid option.");
		}
	}
	argc -= optind;
	argv += optind;

	/* initialize server defaults */
	for (i = 0 ; i < MAX_SERVERS ; i++)
		server_init(&servers[i]);

	/* parse config file */
	if (!conf_file)
		conf_file = strdup(DEFAULT_CONFIG);
	if (parse_config(conf_file, servers, &n_servers, &log))
		errx(1, "error parsing config file.");

	/* log */
	log_init(&log);

#ifdef DEBUG
	for (i = 0 ; i < n_servers ; i++)
		print_info(&servers[i]);
#endif

	if (!n_servers)
		return 0;

	/* allocate server resources */
	for (i = 0 ; i < n_servers ; i++)
		server_postinit(&servers[i]);

	/* initialize event queue */
	if ((ev_queue = kqueue()) == -1)
		errx(1, "kqueue");


	gettimeofday(&start, NULL);
	a = (unsigned long long)(start.tv_sec) * 1000 + (unsigned long long)(start.tv_usec) / 1000;

	for (i = 0 ; i < n_servers ; i++)
		events_init(ev_queue, &servers[i], &log);

	/* event loop */
#ifdef DEBUG
	printf("[INFO] event_loop.\n");
#endif
	while (n_servers) {
		n = kevent(ev_queue, NULL, 0, &kev, 1, NULL);
		if (kev.filter == EVFILT_READ) {
			s = (int)(kev.ident);
			i = (int)(((struct server *)(kev.udata))->id);
			sp = (struct server *)kev.udata;
			if (s == servers[i].sskt && servers[i].transport == SRVT_TCP) {	/* new connection */
#ifdef DEBUG
				printf("[INFO] new tcp connection at server %d.\n", servers[i].id);
#endif
				client_skt = accept(servers[i].sskt, (struct sockaddr *)&sw_addr, &addr_len);
				if (fcntl(client_skt, F_SETFL, O_NONBLOCK) == -1)
					err(1, "fcntl");
				EV_SET(&kev, client_skt, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, (void *)&servers[i]);
				if ((kevent(ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
					err(1, "kevent");
			} else {	/* new packet */
#ifdef DEBUG
				printf("[INFO] new packet at server %d.\n", servers[i].id);
#endif
				gettimeofday(&now, NULL);
				b = (unsigned long long)(now.tv_sec) * 1000 + (unsigned long long)(now.tv_usec) / 1000;
				evtm = (double)(b - a) / 1000;
				if (servers[i].transport == SRVT_TCP) {
#ifdef DEBUG
					ch = (int)kev.data;
					printf("[DEBUG] supposed to read %d.\n", ch);
					size_t solen;
					ioctl(s, FIONREAD, &solen);
					printf("[DEBUG] fioread len %d.\n", solen);
#endif
					bytes = read(s, servers[i].buf, servers[i].buflen);
					if (bytes == 0) {
#ifdef DEBUG
						printf("[INFO] connection closed at server %d.\n", servers[i].id);
#endif
						EV_SET(&kev, s, EVFILT_READ, EV_DELETE, 0, 0, NULL);
						if ((kevent(ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
							err(1, "kevent");
						close(s);
					} else {
						addr_len = sizeof(struct sockaddr_storage);
						getpeername(s, (struct sockaddr *)&sw_addr, &addr_len);
						ip = strdup(inet_ntoa(((struct sockaddr_in *)&sw_addr)->sin_addr));
						f_log(&log, "%lf, %d, %s, %zd\n", evtm, servers[i].id, ip, bytes); 
						fflush(log.fd);
					}
				} else if (servers[i].transport == SRVT_UDP) {
					addr_len = sizeof(struct sockaddr_storage);
#ifdef DEBUG
					ch = (int)kev.data;
					printf("[DEBUG] supposed to read %d.\n", ch);
					size_t solen;
					ioctl(s, FIONREAD, &solen);
					printf("[DEBUG] fioread len %d.\n", solen);
#endif
					bytes = recvfrom(s, servers[i].buf, servers[i].buflen, 0, (struct sockaddr *)&sw_addr, &addr_len);
					if (bytes == -1)
						warn("recvfrom");
					ip = strdup(inet_ntoa(((struct sockaddr_in *)&sw_addr)->sin_addr));
					f_log(&log, "%lf, %d, %s, %zd\n", evtm, servers[i].id, ip, bytes);
					fflush(log.fd);
					free(ip);
				}
			}
		} else if (kev.filter == EVFILT_TIMER) {
			i = (int)kev.ident;
			if (!servers[i].started) {
				server_start(ev_queue, &servers[i]);
			}
			else {
				server_stop(ev_queue, &servers[i]);
				--n_servers;
			}
		}
	}

	log_free(&log);
	free(conf_file);

	return 0;
}
