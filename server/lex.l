/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


%{
#include <stdio.h>
#include "y.tab.h"

extern int yyerror(const char *fmt, ...);
%}

%option yylineno
%option noyywrap

%x dquotes

%%

at				{ return AT; }
to				{ return TO; }
for				{ return FOR; }
is				{ return IS; }
and				{ return AND; }
with				{ return WITH; }

listen				{ return LISTEN; }
on				{ return ON; }
port				{ return PORT; }

transport			{ return TRANSPORT; }
udp				{ return UDP; }
tcp				{ return TCP; }

buffer				{ return BUFFER; }
size				{ return SIZE; }

log				{ return LOG; }
csv				{ return CSV; }


\{				{ return LCBRACKET;}
\}				{ return RCBRACKET;}

[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+	{ yylval.s = strdup(yytext); return IPADDR; }	/* XXX: do better than that! */
[0-9]+\.[0-9]+			{ yylval.d = strtod(yytext, NULL); return REAL; }
[0-9]+				{ yylval.i = atoi(yytext); return INTEGER; }

\"				{ BEGIN(dquotes); }
<dquotes>[^\"]*\"		{
					yylval.s = strndup(yytext, yyleng - 1);
					BEGIN(INITIAL);
					return STRING;
				}

[ \t\n]				{ ; }
\#.*\n				{ ; }
.				{ yyerror("error: invalid character: %s", yytext); }

%%
