SUBDIR=		client server

MAN=		flowgen.1
MLINKS=		flowgend.1

CFLAGS+=	-g
COPTS+=		-Wall -DDEBUG

.include <bsd.subdir.mk>
