LEX=lex
CFLAGS+= -Wall -I/usr/include/kqueue -I. -g -DDEBUG
SRCS=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SRCS))
all: server client
%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)
server: $(OBJS)
	$(MAKE) -C server -f Makefile.gnu
client: $(OBJS)
	$(MAKE) -C client -f Makefile.gnu
install: all
	install -m 0555 server/flowgend /usr/sbin/flowgend
	install -m 0555 client/flowgen /usr/bin/flowgen
	install -m 0444 flowgen.1 /usr/share/man/man1/flowgen.1
	ln -sf /usr/share/man/man1/flowgen.1 /usr/share/man/man1/flowgend.1
clean:
	$(MAKE) -C server clean -f Makefile.gnu
	$(MAKE) -C client clean -f Makefile.gnu
	rm -f *.o
